package pl.rewards.program.domain.exception;

public class RewardsProgramException extends RuntimeException {

    public RewardsProgramException(String message) {
        super(message);
    }
}
