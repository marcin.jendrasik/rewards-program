package pl.rewards.program.domain.transaction.service;

import pl.rewards.program.domain.transaction.mapper.TransactionDomainMapper;
import pl.rewards.program.ports.transaction.dto.CreateTransaction;
import pl.rewards.program.domain.transaction.TransactionDomain;
import pl.rewards.program.ports.transaction.TransactionRepository;
import pl.rewards.program.ports.transaction.TransactionService;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.util.logging.Logger;

public class TransactionServiceImpl implements TransactionService {

    public static final Logger LOGGER = Logger.getLogger(TransactionServiceImpl.class.getSimpleName());
    private final TransactionRepository repository;

    public TransactionServiceImpl(TransactionRepository repository) {
        this.repository = repository;
    }

    public Transaction create(final CreateTransaction dto) {
        final TransactionDomain transaction = TransactionDomainMapper.toDomain(dto);

        LOGGER.info("Domain model before save: %s".formatted(transaction));

        final Transaction saved = repository.save(TransactionDomainMapper.toEntity(transaction));

        LOGGER.info("Domain model after save: %s".formatted(saved));
        return saved;
    }
}
