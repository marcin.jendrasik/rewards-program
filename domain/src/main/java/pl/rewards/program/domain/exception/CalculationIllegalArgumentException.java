package pl.rewards.program.domain.exception;

public class CalculationIllegalArgumentException extends RewardsProgramException {

    public CalculationIllegalArgumentException(String message) {
        super(message);
    }
}
