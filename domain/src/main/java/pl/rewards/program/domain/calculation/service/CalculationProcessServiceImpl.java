package pl.rewards.program.domain.calculation.service;

import pl.rewards.program.common.page.CommonPage;
import pl.rewards.program.domain.calculation.mapper.CalculationDomainMapper;
import pl.rewards.program.ports.calculation.dto.Calculation;
import pl.rewards.program.ports.calculation.dto.ReadCalculation;
import pl.rewards.program.domain.calculation.CalculationDomain;
import pl.rewards.program.ports.calculation.CalculationProcessService;
import pl.rewards.program.ports.transaction.TransactionRepository;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class CalculationProcessServiceImpl implements CalculationProcessService {

    private static final Logger LOGGER = Logger.getLogger(CalculationProcessServiceImpl.class.getSimpleName());
    public static final int BATCHES = 50;
    private final TransactionRepository transactionRepository;
    private final CalculationService calculationService;

    public CalculationProcessServiceImpl(TransactionRepository transactionRepository, CalculationService calculationService) {
        this.transactionRepository = transactionRepository;
        this.calculationService = calculationService;
    }

    @Override
    public Calculation calculate(ReadCalculation dto) {
        final Long clientId = dto.getClientId();
        final Instant dateFrom = dto.getDateFrom();
        final Instant dateTo = dto.getDateTo();

        List<BigDecimal> rewardsToSum = new ArrayList<>();
        List<BigDecimal> purchasesToSum = new ArrayList<>();

        final int size = transactionRepository.countByClientIdAndExternalDateIsBetween(clientId, dateFrom, dateTo);
        LOGGER.info("Found total: %s transactions for clientId: %s".formatted(size, clientId));

        final int batches = size / BATCHES;
        if (batches > 0) {
            IntStream.range(0, batches).forEach(i -> {
                final List<BigDecimal> purchases = transactionRepository.findAllByClientIdAndExternalDateIsBetween(clientId, dateFrom, dateTo, new CommonPage(i, BATCHES))
                        .stream()
                        .map(Transaction::getPurchase)
                        .toList();
                rewardsToSum.add(calculationService.calculate(purchases));
                purchasesToSum.add(sum(purchases));
            });
        } else if (size > 0) {
            final List<BigDecimal> purchases = transactionRepository.findAllByClientIdAndExternalDateIsBetween(clientId, dateFrom, dateTo, new CommonPage(0, size))
                    .stream()
                    .map(Transaction::getPurchase)
                    .toList();
            rewardsToSum.add(calculationService.calculate(purchases));
            purchasesToSum.add(sum(purchases));
        }

        final CalculationDomain calculation = CalculationDomain.builder()
                .clientId(clientId)
                .dateFrom(dateFrom)
                .dateTo(dateTo)
                .rewardsSum(sum(rewardsToSum))
                .purchasesSum(sum(purchasesToSum))
                .build();
        LOGGER.info("Domain object calculation: %s".formatted(calculation));

        return CalculationDomainMapper.toEntity(calculation);
    }

    private BigDecimal sum(List<BigDecimal> values) {
        return values.stream().reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }
}
