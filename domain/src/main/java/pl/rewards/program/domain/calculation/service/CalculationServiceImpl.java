package pl.rewards.program.domain.calculation.service;

import pl.rewards.program.domain.exception.CalculationIllegalArgumentException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CalculationServiceImpl implements CalculationService {

    public static final BigDecimal FIRST_THRESHOLD_REWARD = BigDecimal.valueOf(50);
    public static final BigDecimal SECOND_THRESHOLD_REWARD = BigDecimal.valueOf(100);

    @Override
    public BigDecimal calculate(BigDecimal value) {
        if (value.compareTo(BigDecimal.ZERO) < 0)
            throw new CalculationIllegalArgumentException(value.toString());

        final BigDecimal valueRoundedDown = value.setScale(0, RoundingMode.FLOOR);

        if (valueRoundedDown.compareTo(FIRST_THRESHOLD_REWARD) <= 0) {
            return BigDecimal.ZERO;
        } else if (valueRoundedDown.compareTo(SECOND_THRESHOLD_REWARD) <= 0) {
            return valueRoundedDown.subtract(FIRST_THRESHOLD_REWARD);
        }

        return calculateForValueGreaterThen100(valueRoundedDown);
    }

    @Override
    public BigDecimal calculate(List<BigDecimal> values) {
        return values.stream()
                .map(this::calculate)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    private BigDecimal calculateForValueGreaterThen100(BigDecimal value) {
        return FIRST_THRESHOLD_REWARD.add(BigDecimal.valueOf(2).multiply(value.subtract(SECOND_THRESHOLD_REWARD)));
    }
}
