package pl.rewards.program.domain.calculation.service;

import java.math.BigDecimal;
import java.util.List;

public interface CalculationService {

    BigDecimal calculate(BigDecimal value);
    BigDecimal calculate(List<BigDecimal> values);
}
