package pl.rewards.program.domain.calculation.mapper;

import pl.rewards.program.domain.calculation.CalculationDomain;
import pl.rewards.program.ports.calculation.dto.Calculation;

import java.util.Optional;

public class CalculationDomainMapper {

    private CalculationDomainMapper() {}

    public static CalculationDomain toDomain(Calculation entity) {
        return Optional.ofNullable(entity)
                .map(e ->
                        CalculationDomain.builder()
                            .clientId(e.getClientId())
                            .dateFrom(e.getDateFrom())
                            .dateTo(e.getDateTo())
                            .purchasesSum(e.getPurchasesSum())
                            .rewardsSum(e.getRewardsSum())
                            .build())
                .orElse(null);
    }

    public static Calculation toEntity(CalculationDomain domain) {
        return Optional.ofNullable(domain)
                .map(d -> Calculation.builder()
                            .clientId(d.getClientId())
                            .dateFrom(d.getDateFrom())
                            .dateTo(d.getDateTo())
                            .purchasesSum(d.getPurchasesSum())
                            .rewardsSum(d.getRewardsSum())
                            .build())
                .orElse(null);
    }
}
