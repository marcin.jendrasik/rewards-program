package pl.rewards.program.domain.transaction.mapper;

import pl.rewards.program.domain.transaction.TransactionDomain;
import pl.rewards.program.ports.transaction.dto.CreateTransaction;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.util.Optional;

public class TransactionDomainMapper {

    private TransactionDomainMapper() {}

    public static TransactionDomain toDomain(CreateTransaction dto) {
        return Optional.ofNullable(dto)
                .map(t -> new TransactionDomain(null, t.getClientId(), t.getExternalDate(), t.getPurchase()))
                .orElse(null);
    }

    public static Transaction toEntity(TransactionDomain transactionDomain) {
        return Optional.ofNullable(transactionDomain)
                .map(t -> Transaction.builder()
                        .id(t.id())
                        .clientId(t.clientId())
                        .externalDate(t.externalDate())
                        .purchase(t.purchase())
                        .build())
                .orElse(null);
    }
}
