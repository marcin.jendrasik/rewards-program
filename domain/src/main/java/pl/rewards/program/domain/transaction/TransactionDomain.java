package pl.rewards.program.domain.transaction;

import java.math.BigDecimal;
import java.time.Instant;

public record TransactionDomain(Long id, Long clientId, Instant externalDate, BigDecimal purchase) {
}
