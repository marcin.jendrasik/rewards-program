package pl.rewards.program.domain.calculation;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class CalculationDomain {

    private Long clientId;
    private Instant dateFrom;
    private Instant dateTo;
    private BigDecimal rewardsSum;
    private BigDecimal purchasesSum;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Instant getDateTo() {
        return dateTo;
    }

    public void setDateTo(Instant dateTo) {
        this.dateTo = dateTo;
    }

    public BigDecimal getRewardsSum() {
        return rewardsSum;
    }

    public void setRewardsSum(BigDecimal rewardsSum) {
        this.rewardsSum = rewardsSum;
    }

    public BigDecimal getPurchasesSum() {
        return purchasesSum;
    }

    public void setPurchasesSum(BigDecimal purchasesSum) {
        this.purchasesSum = purchasesSum;
    }

    public static CalculationBuilder builder() {
        return new CalculationBuilder();
    }
    public static final class CalculationBuilder {
        private Long clientId;
        private Instant dateFrom;
        private Instant dateTo;
        private BigDecimal rewardsSum;
        private BigDecimal purchasesSum;

        private CalculationBuilder() {
        }

        public CalculationBuilder clientId(Long clientId) {
            this.clientId = clientId;
            return this;
        }

        public CalculationBuilder dateFrom(Instant dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public CalculationBuilder dateTo(Instant dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public CalculationBuilder rewardsSum(BigDecimal rewardsSum) {
            this.rewardsSum = rewardsSum;
            return this;
        }

        public CalculationBuilder purchasesSum(BigDecimal purchasesSum) {
            this.purchasesSum = purchasesSum;
            return this;
        }

        public CalculationDomain build() {
            CalculationDomain calculation = new CalculationDomain();
            calculation.setClientId(clientId);
            calculation.setDateFrom(dateFrom);
            calculation.setDateTo(dateTo);
            calculation.setRewardsSum(rewardsSum);
            calculation.setPurchasesSum(purchasesSum);
            return calculation;
        }
    }

    @Override
    public String toString() {
        return "Calculation{" +
                "clientId=" + clientId +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", rewardsSum=" + rewardsSum +
                ", purchasesSum=" + purchasesSum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculationDomain that = (CalculationDomain) o;
        return clientId.equals(that.clientId) && dateFrom.equals(that.dateFrom) && dateTo.equals(that.dateTo) && rewardsSum.equals(that.rewardsSum) && purchasesSum.equals(that.purchasesSum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, dateFrom, dateTo, rewardsSum, purchasesSum);
    }
}
