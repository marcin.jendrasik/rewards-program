package pl.rewards.program.domain.calculation.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.rewards.program.domain.calculation.service.CalculationServiceImpl;
import pl.rewards.program.domain.exception.CalculationIllegalArgumentException;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class CalculationServiceImplTest {
    @InjectMocks
    private CalculationServiceImpl calculationService;

    @Test
    void given_value_when_less_then_0_throw_exception() {
        //given
        BigDecimal value = BigDecimal.valueOf(-1);

        //when
        final CalculationIllegalArgumentException illegalArgumentException = assertThrows(CalculationIllegalArgumentException.class,
                () -> calculationService.calculate(value));

        //then
        assertEquals("-1", illegalArgumentException.getMessage());
    }
    private static Stream<Arguments> provideValuesForNumbersLessOrEqual50() {
        return Stream.of(
                Arguments.of(BigDecimal.valueOf(0), BigDecimal.valueOf(0)),
                Arguments.of(BigDecimal.valueOf(40.01), BigDecimal.valueOf(0)),
                Arguments.of(BigDecimal.valueOf(50.4), BigDecimal.valueOf(0))
        );
    }

    @ParameterizedTest
    @MethodSource("provideValuesForNumbersLessOrEqual50")
    void given_value_when_is_less_or_equal_50_then_return_dont_give_rewards(BigDecimal value, BigDecimal expected) {
        //when
        final BigDecimal result = calculationService.calculate(value);

        //then
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideValuesForNumbersGreaterThen50AndLessOrEqual100() {
        return Stream.of(
                Arguments.of(BigDecimal.valueOf(54.01), BigDecimal.valueOf(4)),
                Arguments.of(BigDecimal.valueOf(65), BigDecimal.valueOf(15)),
                Arguments.of(BigDecimal.valueOf(100.10), BigDecimal.valueOf(50))
        );
    }

    @ParameterizedTest
    @MethodSource("provideValuesForNumbersGreaterThen50AndLessOrEqual100")
    void given_value_when_is_greater_then_50_and_less_or_equal_100_then_give_rewards_1_point_for_each_value(BigDecimal value, BigDecimal expected) {
        //when
        final BigDecimal result = calculationService.calculate(value);

        //then
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideValuesForNumbersGreaterThen100() {
        return Stream.of(
                Arguments.of(BigDecimal.valueOf(101), BigDecimal.valueOf(52)),
                Arguments.of(BigDecimal.valueOf(120.10), BigDecimal.valueOf(90)),
                Arguments.of(BigDecimal.valueOf(150.01), BigDecimal.valueOf(150))
        );
    }

    @ParameterizedTest
    @MethodSource("provideValuesForNumbersGreaterThen100")
    void given_value_when_is_greater_then_100_then_give_rewards_1_point_from_50_to_100_and_give_2_points_from_100_for_each_value(BigDecimal value, BigDecimal expected) {
        //when
        final BigDecimal result = calculationService.calculate(value);

        //then
        assertEquals(expected, result);
    }
}