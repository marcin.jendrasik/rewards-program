package pl.rewards.program.ports.calculation.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@Builder
@Getter
@ToString
public class Calculation {

    private Long clientId;
    private Instant dateFrom;
    private Instant dateTo;
    private BigDecimal rewardsSum;
    private BigDecimal purchasesSum;
}
