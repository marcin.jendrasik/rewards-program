package pl.rewards.program.ports.calculation;

import pl.rewards.program.ports.calculation.dto.Calculation;
import pl.rewards.program.ports.calculation.dto.ReadCalculation;

public interface CalculationProcessService {

    Calculation calculate(ReadCalculation dto);
}
