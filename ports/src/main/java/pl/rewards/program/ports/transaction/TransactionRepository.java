package pl.rewards.program.ports.transaction;

import pl.rewards.program.common.page.CommonPage;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.time.Instant;
import java.util.List;

public interface TransactionRepository {

    Transaction save(Transaction transaction);
    List<Transaction> findAllByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo, CommonPage commonPage);
    int countByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo);
}