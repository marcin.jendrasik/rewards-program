package pl.rewards.program.ports.transaction.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class CreateTransaction {

    private Long clientId;
    private Instant externalDate;
    private BigDecimal purchase;
}
