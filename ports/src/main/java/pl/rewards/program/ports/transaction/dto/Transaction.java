package pl.rewards.program.ports.transaction.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@Builder
@Getter
@ToString
public class Transaction {

    private Long id;
    private Long clientId;
    private Instant externalDate;
    private BigDecimal purchase;
}
