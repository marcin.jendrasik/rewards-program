package pl.rewards.program.ports.calculation.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
public class ReadCalculation {

    private Long clientId;
    private Instant dateFrom;
    private Instant dateTo;
}
