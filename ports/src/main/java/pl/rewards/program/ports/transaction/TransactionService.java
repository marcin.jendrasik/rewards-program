package pl.rewards.program.ports.transaction;

import pl.rewards.program.ports.transaction.dto.CreateTransaction;
import pl.rewards.program.ports.transaction.dto.Transaction;

public interface TransactionService {

    Transaction create(final CreateTransaction dto);
}
