package pl.rewards.program.common.mapper;

public interface CommonMapper<D, E> {

    D toDomain(E entity);
    E toEntity(D domain);
}
