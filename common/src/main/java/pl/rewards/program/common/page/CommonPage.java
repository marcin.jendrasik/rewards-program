package pl.rewards.program.common.page;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CommonPage {

    int number;
    int size;
}
