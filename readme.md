# Rewards Program

## How it works:
In my application, we have two endpoints:
1. Creating transaction
2. Calculating rewards for specific range of date for particular customer

## Application is separated on 5 main modules:
The main purpose was to apply Clean Architection pattern to separate core from framework and also testing independently. That kind of architecting is hexagonal architection consists of:
- enterprise business rules
- application business rules
- ports, adapters
- frameworks

### Domain:
Main business logic written in pure Java, totally separated from other layers. This is the core of our application. Doesn't know anything about other layers.

### Ports:
Basically, are interfaces implemented by business and adapters (like framework things)

### Adapters:
Here we have some framework components which we can divide them on inputs:controllers and outputs: repositories. Generally, we can divide them on a lot of more.

### Common:
Useable stuff for every module

### Service:
Base module responsible for running application, which has also bean's configuration in goal of creation of domain components.

## Running application:

1. Java version: 17
2. In cause of using generating mappers, we need to first run:
```bash
mvn clean install 
```

3. Go to service module folder:
```bash 
cd service
```

4. Run:
```bash 
mvn spring-boot:run
```

## Testing:
### In the project, we have some tests in domain and adapters modules.
#### Domain:
Relevant part is unit testing calculation rewards gained by customer

#### Adapters:
Unit tests for controllers. Integration tests for checking functionality of whole way (endpoints).

### Manually (IntelliJ):
In the file requests.http located in the project root are two endpoints:
1. One for calculating stuff GET
2. One for create transaction resource

What we only need is click green triangle(play) on the bar located on the left side or select whole request, right click and play

### Automating:
For particular module e.g.: domain
```bash
mvn test -pl domain -am
```
For all:
```bash
mvn test
```

submodule is: adapters, domain