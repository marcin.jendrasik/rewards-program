package pl.rewards.program;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import pl.rewards.program.adapters.transactional.calculation.CalculationProcessTransactionalDecoratorServiceImpl;
import pl.rewards.program.adapters.transactional.transaction.TransactionServiceTransactionalDecoratorImpl;
import pl.rewards.program.domain.calculation.service.CalculationProcessServiceImpl;
import pl.rewards.program.domain.calculation.service.CalculationService;
import pl.rewards.program.domain.calculation.service.CalculationServiceImpl;
import pl.rewards.program.domain.transaction.service.TransactionServiceImpl;
import pl.rewards.program.ports.calculation.CalculationProcessService;
import pl.rewards.program.ports.transaction.TransactionRepository;
import pl.rewards.program.ports.transaction.TransactionService;

@Configuration
public class RewardsProgramConfiguration {

    final TransactionRepository transactionRepository;

    public RewardsProgramConfiguration(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Bean
    TransactionService creatTransactionService() {
        return new TransactionServiceImpl(transactionRepository);
    }

    @Bean
    @Primary
    TransactionService creatTransactionTransactionalDecoratorService() {
        return new TransactionServiceTransactionalDecoratorImpl(creatTransactionService());
    }

    @Bean
    CalculationService createCalculationService() {
        return new CalculationServiceImpl();
    }

    @Bean
    CalculationProcessService createCalculationProcessService() {
        return new CalculationProcessServiceImpl(transactionRepository, createCalculationService());
    }


    @Bean
    @Primary
    CalculationProcessService createCalculationProcessTransactionalDecoratorService() {
        return new CalculationProcessTransactionalDecoratorServiceImpl(createCalculationProcessService());
    }
}
