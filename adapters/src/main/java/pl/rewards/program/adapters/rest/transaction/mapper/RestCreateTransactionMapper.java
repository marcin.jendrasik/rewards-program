package pl.rewards.program.adapters.rest.transaction.mapper;

import org.mapstruct.Mapper;
import pl.rewards.program.adapters.rest.transaction.dto.CreateTransactionDto;
import pl.rewards.program.common.mapper.CommonMapper;
import pl.rewards.program.ports.transaction.dto.CreateTransaction;

@Mapper(componentModel = "spring")
public interface RestCreateTransactionMapper extends CommonMapper<CreateTransactionDto, CreateTransaction> {
}
