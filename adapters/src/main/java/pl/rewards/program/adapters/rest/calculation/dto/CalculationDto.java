package pl.rewards.program.adapters.rest.calculation.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
public class CalculationDto {

    private Long clientId;
    private Instant dateFrom;
    private Instant dateTo;
    private BigDecimal rewardsSum;
    private BigDecimal purchasesSum;
}
