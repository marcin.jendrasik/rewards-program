package pl.rewards.program.adapters.rest.transaction.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
public class TransactionDto {

    private Long id;
    private Long clientId;
    private Instant externalDate;
    private BigDecimal purchase;
}
