package pl.rewards.program.adapters.transaction;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

interface TransactionJpaRepository extends JpaRepository<TransactionEntity, Long> {

    List<TransactionEntity> findAllByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo, Pageable pageable);
    int countAllByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo);
}
