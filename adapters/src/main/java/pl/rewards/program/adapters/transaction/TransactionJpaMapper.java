package pl.rewards.program.adapters.transaction;

import org.mapstruct.Mapper;
import pl.rewards.program.common.mapper.CommonMapper;
import pl.rewards.program.ports.transaction.dto.Transaction;

@Mapper(componentModel = "spring")
public interface TransactionJpaMapper extends CommonMapper<Transaction, TransactionEntity> {
}
