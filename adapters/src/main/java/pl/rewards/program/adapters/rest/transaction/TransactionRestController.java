package pl.rewards.program.adapters.rest.transaction;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.rewards.program.adapters.rest.transaction.dto.CreateTransactionDto;
import pl.rewards.program.adapters.rest.transaction.dto.TransactionDto;
import pl.rewards.program.adapters.rest.transaction.mapper.RestCreateTransactionMapper;
import pl.rewards.program.adapters.rest.transaction.mapper.RestTransactionMapper;
import pl.rewards.program.ports.transaction.TransactionService;
import pl.rewards.program.ports.transaction.dto.Transaction;

@RestController
@RequestMapping("/api/transaction")
@RequiredArgsConstructor
@Log4j2
public class TransactionRestController {

    private final TransactionService transactionService;
    private final RestTransactionMapper restTransactionMapper;
    private final RestCreateTransactionMapper restCreateTransactionMapper;

    @PostMapping
    public ResponseEntity<TransactionDto> create(final @Valid @RequestBody CreateTransactionDto createTransaction) {
        log.info("Input data: " + createTransaction);
        final Transaction transaction = transactionService.create(restCreateTransactionMapper.toEntity(createTransaction));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(restTransactionMapper.toEntity(transaction));
    }
}
