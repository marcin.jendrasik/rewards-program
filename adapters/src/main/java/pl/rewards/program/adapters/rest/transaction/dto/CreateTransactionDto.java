package pl.rewards.program.adapters.rest.transaction.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class CreateTransactionDto {

    @NotNull
    private Long clientId;
    @NotNull
    private Instant externalDate;
    @NotNull
    @Min(value = 1L)
    private BigDecimal purchase;
}
