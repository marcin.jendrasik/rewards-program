package pl.rewards.program.adapters.transactional.calculation;

import org.springframework.transaction.annotation.Transactional;
import pl.rewards.program.ports.calculation.dto.Calculation;
import pl.rewards.program.ports.calculation.dto.ReadCalculation;
import pl.rewards.program.ports.calculation.CalculationProcessService;

@Transactional
public class CalculationProcessTransactionalDecoratorServiceImpl implements CalculationProcessService {

    private final CalculationProcessService calculationProcessService;

    public CalculationProcessTransactionalDecoratorServiceImpl(CalculationProcessService calculationProcessService) {
        this.calculationProcessService = calculationProcessService;
    }

    @Override
    @Transactional(readOnly = true)
    public Calculation calculate(ReadCalculation dto) {
        return calculationProcessService.calculate(dto);
    }
}
