package pl.rewards.program.adapters.rest.calculation;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.rewards.program.adapters.rest.calculation.dto.CalculationDto;
import pl.rewards.program.adapters.rest.calculation.mapper.RestCalculationMapper;
import pl.rewards.program.ports.calculation.dto.Calculation;
import pl.rewards.program.ports.calculation.dto.ReadCalculation;
import pl.rewards.program.ports.calculation.CalculationProcessService;

import java.time.Instant;

@RestController
@RequestMapping("/api/calculation")
@RequiredArgsConstructor
@Log4j2
public class CalculationRestController {

    private final CalculationProcessService calculationProcessService;
    private final RestCalculationMapper mapper;

    @GetMapping
    public ResponseEntity<CalculationDto> calculate(
            @RequestParam(name = "clientId") final Long clientId,
            @RequestParam(name = "dateFrom") final @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant dateFrom,
            @RequestParam(name = "dateTo", required = false) final @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant dateTo
    ) {
        log.info("Input parameters: clientId: %s, dateFrom: %s, dateTo: %s".formatted(clientId, dateFrom, dateTo));
        final Calculation calculate = calculationProcessService.calculate(ReadCalculation.builder()
                        .clientId(clientId)
                        .dateFrom(dateFrom)
                        .dateTo(dateTo)
                        .build());
        return ResponseEntity.ok(mapper.toEntity(calculate));
    }
}
