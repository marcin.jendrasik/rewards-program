package pl.rewards.program.adapters.transactional.transaction;

import org.springframework.transaction.annotation.Transactional;
import pl.rewards.program.ports.transaction.dto.CreateTransaction;
import pl.rewards.program.ports.transaction.TransactionService;
import pl.rewards.program.ports.transaction.dto.Transaction;

@Transactional
public class TransactionServiceTransactionalDecoratorImpl implements TransactionService {

    private final TransactionService transactionService;

    public TransactionServiceTransactionalDecoratorImpl(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Transactional
    public Transaction create(final CreateTransaction dto) {
        return transactionService.create(dto);
    }
}
