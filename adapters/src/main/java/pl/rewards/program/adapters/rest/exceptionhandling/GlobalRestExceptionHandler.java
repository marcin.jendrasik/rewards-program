package pl.rewards.program.adapters.rest.exceptionhandling;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.rewards.program.domain.exception.CalculationIllegalArgumentException;

import java.util.Locale;

import static java.util.stream.Collectors.joining;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@RequiredArgsConstructor
@Log4j2
public class GlobalRestExceptionHandler {
    private static final String KEY_VALUE_SEPARATOR = " ";
    private static final String DELIMITER = ", ";

    private final RestExceptionResponseBuilder responseBuilder;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionDto> onException(Exception e, Locale locale) {
        log.error("Internal Server Error:", e);
        return responseBuilder.build(e, INTERNAL_SERVER_ERROR, locale);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionDto> onMethodArgumentNotValidException(MethodArgumentNotValidException e, Locale locale) {
        log.error("Validation error:", e);
        final String description = responseBuilder.getDescription(e, locale, getValidationErrors(e));
        return responseBuilder.build(description, BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionDto> onCalculationIllegalArgumentException(CalculationIllegalArgumentException e, Locale locale) {
        log.error("Calculation error:", e);
        final String description = responseBuilder.getDescription(e, locale, e.getMessage());
        return responseBuilder.build(description, INTERNAL_SERVER_ERROR);
    }

    private String getValidationErrors(MethodArgumentNotValidException exception) {
        return exception.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> fieldError.getField() + KEY_VALUE_SEPARATOR + fieldError.getDefaultMessage())
                .collect(joining(DELIMITER));
    }
}
