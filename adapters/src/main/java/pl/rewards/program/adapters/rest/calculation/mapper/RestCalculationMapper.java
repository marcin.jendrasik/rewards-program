package pl.rewards.program.adapters.rest.calculation.mapper;

import org.mapstruct.Mapper;
import pl.rewards.program.adapters.rest.calculation.dto.CalculationDto;
import pl.rewards.program.common.mapper.CommonMapper;
import pl.rewards.program.ports.calculation.dto.Calculation;

@Mapper(componentModel = "spring")
public interface RestCalculationMapper extends CommonMapper<Calculation, CalculationDto> {
}
