package pl.rewards.program.adapters.rest.transaction.mapper;

import org.mapstruct.Mapper;
import pl.rewards.program.adapters.rest.transaction.dto.TransactionDto;
import pl.rewards.program.common.mapper.CommonMapper;
import pl.rewards.program.ports.transaction.dto.Transaction;

@Mapper(componentModel = "spring")
public interface RestTransactionMapper extends CommonMapper<Transaction, TransactionDto> {
}
