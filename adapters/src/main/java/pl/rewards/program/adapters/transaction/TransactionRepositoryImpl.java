package pl.rewards.program.adapters.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import pl.rewards.program.common.page.CommonPage;
import pl.rewards.program.ports.transaction.TransactionRepository;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.time.Instant;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class TransactionRepositoryImpl implements TransactionRepository {

    private final TransactionJpaRepository repository;
    private final TransactionJpaMapper mapper;

    @Override
    public Transaction save(Transaction transaction) {
        return mapper.toDomain(repository.save(mapper.toEntity(transaction)));
    }

    @Override
    public List<Transaction> findAllByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo, CommonPage commonPage) {
        return repository.findAllByClientIdAndExternalDateIsBetween(clientId, dateFrom, dateTo, PageRequest.of(commonPage.getNumber(), commonPage.getSize())).stream()
                .map(mapper::toDomain)
                .toList();
    }

    @Override
    public int countByClientIdAndExternalDateIsBetween(Long clientId, Instant dateFrom, Instant dateTo) {
        return repository.countAllByClientIdAndExternalDateIsBetween(clientId, dateFrom, dateTo);
    }
}
