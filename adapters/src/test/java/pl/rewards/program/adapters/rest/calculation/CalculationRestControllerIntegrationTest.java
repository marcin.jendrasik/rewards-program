package pl.rewards.program.adapters.rest.calculation;

import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import pl.rewards.program.common.mapper.ObjectMapperWrapper;
import pl.rewards.program.adapters.rest.calculation.dto.CalculationDto;
import pl.rewards.program.fixture.CalculationFixture;
import pl.rewards.program.fixture.TransactionFixture;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class CalculationRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Test
    void given_1_entities_and_parameter_when_get_then_return_calculation() throws Exception {
        //given
        entityManager.persist(TransactionFixture.createTransactionEntity(BigDecimal.valueOf(50.4)));
        entityManager.flush();

        final Instant dateFrom = CalculationFixture.DATE_FROM.minus(1, ChronoUnit.DAYS);
        final Instant dateTo = CalculationFixture.DATE_TO.plus(1, ChronoUnit.DAYS);
        final LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
        linkedMultiValueMap.add("clientId", String.valueOf(CalculationFixture.CLIENT_ID));
        linkedMultiValueMap.add("dateFrom", dateFrom.toString());
        linkedMultiValueMap.add("dateTo", dateTo.toString());

        //when
        final MvcResult mvcResult = mockMvc.perform(get("/api/calculation")
                        .params(linkedMultiValueMap))
                .andExpect(status().isOk())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final CalculationDto calculationDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, CalculationDto.class);

        //then
        assertEquals(CalculationFixture.CLIENT_ID, calculationDto.getClientId());
        assertEquals(dateFrom, calculationDto.getDateFrom());
        assertEquals(dateTo, calculationDto.getDateTo());
        assertEquals(BigDecimal.valueOf(50.4), calculationDto.getPurchasesSum());
        assertEquals(BigDecimal.valueOf(0), calculationDto.getRewardsSum());
    }

    @Transactional
    @Test
    void given_3_entities_and_parameters_when_get_then_return_calculation() throws Exception {
        //given
        entityManager.persist(TransactionFixture.createTransactionEntity(BigDecimal.valueOf(50.01)));
        entityManager.persist(TransactionFixture.createTransactionEntity(BigDecimal.valueOf(100)));
        entityManager.persist(TransactionFixture.createTransactionEntity(BigDecimal.valueOf(120)));
        entityManager.flush();

        final Instant dateFrom = CalculationFixture.DATE_FROM.minus(1, ChronoUnit.DAYS);
        final Instant dateTo = CalculationFixture.DATE_TO.plus(1, ChronoUnit.DAYS);
        final LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
        linkedMultiValueMap.add("clientId", String.valueOf(CalculationFixture.CLIENT_ID));
        linkedMultiValueMap.add("dateFrom", dateFrom.toString());
        linkedMultiValueMap.add("dateTo", dateTo.toString());

        //when
        final MvcResult mvcResult = mockMvc.perform(get("/api/calculation")
                        .params(linkedMultiValueMap))
                .andExpect(status().isOk())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final CalculationDto calculationDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, CalculationDto.class);

        //then
        assertEquals(CalculationFixture.CLIENT_ID, calculationDto.getClientId());
        assertEquals(dateFrom, calculationDto.getDateFrom());
        assertEquals(dateTo, calculationDto.getDateTo());
        assertEquals(BigDecimal.valueOf(270.01), calculationDto.getPurchasesSum());
        assertEquals(BigDecimal.valueOf(140), calculationDto.getRewardsSum());
    }

    @Transactional
    @Test
    void given_100_entities_and_parameters_when_get_then_return_calculation() throws Exception {
        //given
        for (int i = 0; i < 100; i++) {
            entityManager.persist(TransactionFixture.createTransactionEntity(BigDecimal.valueOf(110)));
        }
        entityManager.flush();

        final Instant dateFrom = CalculationFixture.DATE_FROM.minus(1, ChronoUnit.DAYS);
        final Instant dateTo = CalculationFixture.DATE_TO.plus(1, ChronoUnit.DAYS);
        final LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
        linkedMultiValueMap.add("clientId", String.valueOf(CalculationFixture.CLIENT_ID));
        linkedMultiValueMap.add("dateFrom", dateFrom.toString());
        linkedMultiValueMap.add("dateTo", dateTo.toString());

        //when
        final MvcResult mvcResult = mockMvc.perform(get("/api/calculation")
                        .params(linkedMultiValueMap))
                .andExpect(status().isOk())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final CalculationDto calculationDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, CalculationDto.class);

        //then
        assertEquals(CalculationFixture.CLIENT_ID, calculationDto.getClientId());
        assertEquals(dateFrom, calculationDto.getDateFrom());
        assertEquals(dateTo, calculationDto.getDateTo());
        assertEquals(BigDecimal.valueOf(11000), calculationDto.getPurchasesSum());
        assertEquals(BigDecimal.valueOf(7000), calculationDto.getRewardsSum());
    }
}