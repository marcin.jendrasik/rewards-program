package pl.rewards.program.adapters.rest.transaction;

import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import pl.rewards.program.adapters.transaction.TransactionEntity;
import pl.rewards.program.common.mapper.ObjectMapperWrapper;
import pl.rewards.program.adapters.rest.transaction.dto.CreateTransactionDto;
import pl.rewards.program.adapters.rest.transaction.dto.TransactionDto;
import pl.rewards.program.fixture.TransactionFixture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class TransactionRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Test
    void given_value_when_create_transaction_then_return_result() throws Exception {
        //given
        final CreateTransactionDto createTransactionDto = TransactionFixture.createCreateTransactionDto();
        final String value = ObjectMapperWrapper.getInstance().writeValueAsString(createTransactionDto);

        //when
        final MvcResult mvcResult = mockMvc.perform(post("/api/transaction")
                .content(value)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final TransactionDto transactionDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, TransactionDto.class);
        final TransactionEntity entity = (TransactionEntity) entityManager.createQuery("select t from transaction t").getResultList().get(0);

        //then
        assertEquals(entity.getId(), transactionDto.getId());
        assertEquals(TransactionFixture.CLIENT_ID, transactionDto.getClientId());
        assertEquals(TransactionFixture.EXTERNAL_DATE, transactionDto.getExternalDate());
        assertEquals(TransactionFixture.PURCHASE, transactionDto.getPurchase());
    }
}