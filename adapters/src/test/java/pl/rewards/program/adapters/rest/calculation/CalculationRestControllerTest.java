package pl.rewards.program.adapters.rest.calculation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import pl.rewards.program.adapters.rest.calculation.mapper.RestCalculationMapper;
import pl.rewards.program.adapters.rest.exceptionhandling.RestExceptionResponseBuilder;
import pl.rewards.program.common.mapper.ObjectMapperWrapper;
import pl.rewards.program.fixture.CalculationFixture;
import pl.rewards.program.adapters.rest.calculation.dto.CalculationDto;
import pl.rewards.program.ports.calculation.CalculationProcessService;
import pl.rewards.program.ports.calculation.dto.Calculation;
import pl.rewards.program.ports.calculation.dto.ReadCalculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CalculationRestController.class)
@ExtendWith(SpringExtension.class)
class CalculationRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CalculationProcessService calculationProcessService;
    @MockBean
    private RestCalculationMapper restCalculationMapper;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public RestExceptionResponseBuilder restExceptionResponseBuilder(MessageSource messageSource) {
            return new RestExceptionResponseBuilder(messageSource);
        }
    }

    @Test
    void given_parameter_when_get_then_return_calculation() throws Exception {
        //given
        final LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
        linkedMultiValueMap.add("clientId", String.valueOf(CalculationFixture.CLIENT_ID));
        linkedMultiValueMap.add("dateFrom", CalculationFixture.DATE_FROM.toString());
        linkedMultiValueMap.add("dateTo", CalculationFixture.DATE_TO.toString());

        //when
        when(calculationProcessService.calculate(any(ReadCalculation.class))).thenReturn(CalculationFixture.createCalculation());
        when(restCalculationMapper.toEntity(any(Calculation.class))).thenReturn(CalculationFixture.createCalculationDto());

        final MvcResult mvcResult = mockMvc.perform(get("/api/calculation")
                        .params(linkedMultiValueMap))
                        .andExpect(status().isOk())
                        .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final CalculationDto calculationDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, CalculationDto.class);

        //then
        assertEquals(CalculationFixture.CLIENT_ID, calculationDto.getClientId());
        assertEquals(CalculationFixture.DATE_FROM, calculationDto.getDateFrom());
        assertEquals(CalculationFixture.DATE_TO, calculationDto.getDateTo());
        assertEquals(CalculationFixture.REWARDS_SUM, calculationDto.getRewardsSum());
        assertEquals(CalculationFixture.PURCHASES_SUM, calculationDto.getPurchasesSum());
    }
}