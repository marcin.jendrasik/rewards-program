package pl.rewards.program.adapters.rest.transaction;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.rewards.program.adapters.rest.exceptionhandling.RestExceptionResponseBuilder;
import pl.rewards.program.adapters.rest.transaction.dto.CreateTransactionDto;
import pl.rewards.program.adapters.rest.transaction.dto.TransactionDto;
import pl.rewards.program.adapters.rest.transaction.mapper.RestCreateTransactionMapper;
import pl.rewards.program.adapters.rest.transaction.mapper.RestCreateTransactionMapperImpl;
import pl.rewards.program.adapters.rest.transaction.mapper.RestTransactionMapper;
import pl.rewards.program.adapters.rest.transaction.mapper.RestTransactionMapperImpl;
import pl.rewards.program.common.mapper.ObjectMapperWrapper;
import pl.rewards.program.fixture.TransactionFixture;
import pl.rewards.program.ports.transaction.TransactionService;
import pl.rewards.program.ports.transaction.dto.CreateTransaction;
import pl.rewards.program.ports.transaction.dto.Transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TransactionRestController.class)
@ExtendWith(SpringExtension.class)
class TransactionRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TransactionService transactionService;
    @MockBean
    private RestTransactionMapper restTransactionMapper;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public RestCreateTransactionMapper RestCreateTransactionMapper() {
            return new RestCreateTransactionMapperImpl();
        }

        @Bean
        public RestExceptionResponseBuilder restExceptionResponseBuilder(MessageSource messageSource) {
            return new RestExceptionResponseBuilder(messageSource);
        }
    }

    @Test
    void given_value_when_create_transaction_then_return_result() throws Exception {
        //given
        final CreateTransactionDto createTransactionDto = TransactionFixture.createCreateTransactionDto();
        final String value = ObjectMapperWrapper.getInstance().writeValueAsString(createTransactionDto);

        //when
        when(transactionService.create(any(CreateTransaction.class))).thenReturn(TransactionFixture.createTransaction());
        when(restTransactionMapper.toEntity(any(Transaction.class))).thenReturn(TransactionFixture.createTransactionDto());

        final MvcResult mvcResult = mockMvc.perform(post("/api/transaction")
                .content(value)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final TransactionDto transactionDto = ObjectMapperWrapper.getInstance().readValue(contentAsString, TransactionDto.class);

        assertEquals(TransactionFixture.ID, transactionDto.getId());
        assertEquals(TransactionFixture.CLIENT_ID, transactionDto.getClientId());
        assertEquals(TransactionFixture.EXTERNAL_DATE, transactionDto.getExternalDate());
        assertEquals(TransactionFixture.PURCHASE, transactionDto.getPurchase());
    }
}