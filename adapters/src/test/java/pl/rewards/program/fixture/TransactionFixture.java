package pl.rewards.program.fixture;

import pl.rewards.program.adapters.rest.transaction.dto.CreateTransactionDto;
import pl.rewards.program.adapters.transaction.TransactionEntity;
import pl.rewards.program.adapters.rest.transaction.dto.TransactionDto;
import pl.rewards.program.ports.transaction.dto.Transaction;

import java.math.BigDecimal;
import java.time.Instant;

public class TransactionFixture {

    public static final long ID = 1L;
    public static final long CLIENT_ID = 2L;
    public static final Instant EXTERNAL_DATE = Instant.now();
    public static final BigDecimal PURCHASE = BigDecimal.valueOf(100);

    private TransactionFixture() {}

    public static TransactionEntity createTransactionEntity(BigDecimal purchase) {
        return TransactionEntity.builder()
                .clientId(CLIENT_ID)
                .externalDate(EXTERNAL_DATE)
                .purchase(purchase)
                .build();
    }

    public static Transaction createTransaction() {
        return Transaction.builder()
                .id(ID)
                .clientId(CLIENT_ID)
                .externalDate(EXTERNAL_DATE)
                .purchase(PURCHASE)
                .build();
    }

    public static TransactionDto createTransactionDto() {
        final TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(ID);
        transactionDto.setClientId(CLIENT_ID);
        transactionDto.setExternalDate(EXTERNAL_DATE);
        transactionDto.setPurchase(PURCHASE);
        return transactionDto;
    }

    public static CreateTransactionDto createCreateTransactionDto() {
        final CreateTransactionDto createTransactionDto = new CreateTransactionDto();
        createTransactionDto.setClientId(CLIENT_ID);
        createTransactionDto.setExternalDate(EXTERNAL_DATE);
        createTransactionDto.setPurchase(PURCHASE);
        return createTransactionDto;
    }
}
