package pl.rewards.program.fixture;

import pl.rewards.program.adapters.rest.calculation.dto.CalculationDto;
import pl.rewards.program.ports.calculation.dto.Calculation;

import java.math.BigDecimal;
import java.time.Instant;

public class CalculationFixture {

    public static final long CLIENT_ID = 2L;
    public static final Instant DATE_FROM = Instant.now();
    public static final Instant DATE_TO = Instant.now();
    public static final BigDecimal REWARDS_SUM = BigDecimal.valueOf(10);
    public static final BigDecimal PURCHASES_SUM = BigDecimal.valueOf(60);
    private CalculationFixture() {}

    public static Calculation createCalculation() {
        return Calculation.builder()
                .clientId(CLIENT_ID)
                .dateFrom(DATE_FROM)
                .dateTo(DATE_TO)
                .rewardsSum(REWARDS_SUM)
                .purchasesSum(PURCHASES_SUM)
                .build();
    }

    public static CalculationDto createCalculationDto() {
        final CalculationDto calculationDto = new CalculationDto();
        calculationDto.setClientId(CLIENT_ID);
        calculationDto.setDateFrom(DATE_FROM);
        calculationDto.setDateTo(DATE_TO);
        calculationDto.setRewardsSum(REWARDS_SUM);
        calculationDto.setPurchasesSum(PURCHASES_SUM);
        return calculationDto;
    }
}
